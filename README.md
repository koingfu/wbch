# wBCH

## Deployed contract addresses

- Mainnet: [0xc4eb62f900ae917f8F86366B4C1727eb526D1275](https://www.smartscan.cash/address/0xc4eb62f900ae917f8F86366B4C1727eb526D1275)
- Amber testnet: [0x21c7FD76B440e443aafb364f31396a3Ea0b3Abe0](https://www.smartscan.cash/address/0x21c7FD76B440e443aafb364f31396a3Ea0b3Abe0)
